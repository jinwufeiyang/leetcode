package stringCode;

public class q709 {

    public static String toLowerCase(String s) {
        StringBuilder lowerStr = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 65 && c <= 90) {
                c = (char) (s.charAt(i) + 32);
            }
            lowerStr.append(c);
        }
        return lowerStr.toString();
    }

    public static void main(String[] args) {
        System.out.println(toLowerCase("HaShs"));
        System.out.println(Integer.valueOf('A'));
        System.out.println(Integer.valueOf('Z'));
    }

}
