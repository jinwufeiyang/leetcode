package easy;

import java.util.Stack;

/**
 * 最小栈
 */
public class q155 {

    private Stack stack;
    private Stack minValue;

    public static void main(String[] args) {
        q155 minStack = new q155();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        System.out.println(minStack.getMin());
        minStack.pop();
        System.out.println(minStack.top());
        System.out.println(minStack.getMin());
        System.out.println(minStack.getMin());

    }

    public q155() {
        stack = new Stack();
        minValue = new Stack();
    }

    public void push(int val) {
        stack.push(val);
        if (minValue.isEmpty()) {
            minValue.push(val);
        } else {
            int value = (int) minValue.peek();
            if (val > value) {
                minValue.push(value);
            } else {
                minValue.push(val);
            }
        }
    }

    public void pop() {
        if (stack.isEmpty()) {
            return;
        }
        stack.pop();
        minValue.pop();
    }

    public int top() {
        if (stack.isEmpty()) {
            throw new RuntimeException("your stack is empty!");
        }
        return (int) stack.peek();
    }

    public int getMin() {
        if (minValue.isEmpty()) {
            throw new RuntimeException("your stack is empty!");
        }
        return (int) minValue.peek();
    }

}
