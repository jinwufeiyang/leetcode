package search;

public class BiSearch {

    public static void main(String[] args) {
        int[] array = {1,3,5,8,9,12,56,78,89,123,134};
        System.out.println(biSearch(array, 0));
    }

    public static int biSearch(int[] array, int result) {
        int l = 0;
        int r = array.length - 1;
        int mid;
        while (l < r) {
            mid = l + (r - l) / 2;
            if (array[mid] == result) {
                return mid + 1;
            } else if (array[mid] > result) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return -1;
    }

}
