package offer;

/**
 * 在一个二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
 * 请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 *
 * 1	2	8	9
 * 2	4	9	12
 * 4	7	10	13
 * 6	8	11	15
 */
public class q04 {


    public static void main(String[] args) {
        // 测试用例
        int array[][] = {{1, 2, 8, 9}, {2, 4, 9, 12}, {4, 7, 10, 13}, {6, 8, 11, 15}};
        int number = 16;
        boolean flag = find(array, number);
        System.out.println(flag);
    }

    private static Boolean find(int[][] arr, int number) {
        boolean flag = false;
        if (arr != null && number != 0) {
            // 获取行数
            int rows = arr.length;
            // 获取列数
            int columns = arr[0].length;

            //当前的行和列
            int currRow = 0;
            int currCol = columns - 1;

            int temp = 0;
            while (currRow <= rows -1 && currCol >= 0) {
                // 获取右上角数值
                temp = arr[currRow][currCol];

                if (temp == number) {
                    // 1如果等于则返回
                    flag = true;
                    break;
                } else if (temp < number) {
                    // 2如果大于当前值，则舍弃当前行数据
                    currRow++;
                } else {
                    // 3如果小于当前值，则舍弃当前列数据
                    currCol--;
                }
            }

        }
        return flag;
    }


}
