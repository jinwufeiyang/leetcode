package offer;

public class RemoveNthFromEnd {

    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode pre = new ListNode(0);
        pre.next = head;
        ListNode start = pre;
        ListNode end = pre;
        while (n != 0) {
            end = end.next;
            n--;
        }
        while (end.getNext() != null) {
            start = start.next;
            end = end.next;
        }
        start.next = start.next.next;
        return pre.next;
    }
}
