package mid;


public class q05 {

    public static void main(String[] args) {
        System.out.println(longestPalindrome("c"));
    }

    /**
     * 暴力破解
     * @param s
     * @return
     */
    public static String longestPalindrome(String s) {
        int max = 0;
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            for (int j = i + 1; j <= s.length(); j++) {
                String str = s.substring(i, j);
                if (isPalindromic(str) && str.length() > max) {
                    result = str;
                    max = str.length();
                }
            }
        }
        return result;
    }


    /**
     * 是否回文字符串
     * @param s
     * @return
     */
    public static boolean isPalindromic(String s) {
        int length = s.length();
        for (int i = 0; i < length / 2; i++) {
            if (s.charAt(i) != s.charAt(length - 1 - i)) {
                return false;
            }
        }
        return true;
    }


}
