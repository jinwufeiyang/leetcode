package mid.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class q18 {

    public static void main(String[] args) {
        int[] array = {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};
        int target = 8;
        long t1 = System.currentTimeMillis();
        List<List<Integer>> listList = letCodeFourSum(array, target);
        System.out.println("total time=" + (System.currentTimeMillis() - t1));
        System.out.println(listList.size());
        for (int i = 0; i < listList.size(); i++) {
            System.out.println(listList.get(i).toString());
        }
    }

    public static List<List<Integer>> letCodeFourSum(int[] nums, int target) {
        Set<List<Integer>> set = new HashSet<>();
        Arrays.sort(nums);

        int length = nums.length;

        for (int i = 0; i < length-3; i++) {
            if(i>0 && nums[i]==nums[i-1]) {
                continue;
            }
            for (int j = i+1; j < length-2; j++) {
                if (j>i+1 && nums[j]==nums[j-1]) {
                    continue;
                }
                int l = j + 1;
                int r = length - 1;

                while (l < r) {
                    int sum = nums[i] + nums[j] + nums[l] + nums[r];
                    if (sum == target) {
                        set.add(Arrays.asList(nums[i], nums[j], nums[l], nums[r]));
                        while (l<r && nums[l] == nums[l+1]) {
                            l++;
                        }
                        while (l<r && nums[r] == nums[r-1]) {
                            r--;
                        }
                        l++;
                        r--;
                    } else if (sum < target) {
//                        while (l<r && nums[l] == nums[l+1]) {
//                            l++;
//                        }
                        l++;
                    } else {
//                        while (l<r && nums[r] == nums[r-1]) {
//                            r--;
//                        }
                        r--;
                    }
                }

            }

        }

        return new ArrayList<>(set);
    }

    public static List<List<Integer>> fourSum(int[] nums, int target) {
        Set<List<Integer>> set = new HashSet<>();

        Arrays.sort(nums);

        int length = nums.length;

        for (int i = 0; i < length - 3; i++) {
            int a = nums[i];
            if (i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            for (int j = i + 1; j < length - 2; j++) {
                int b = nums[j];
                for (int k = j + 1; k < length - 1; k++) {
                    int c = nums[k];
                    for (int m = k + 1; m < length; m++) {
                        int d = nums[m];
                        int sum = a + b + c + d;
                        if (sum == target) {
                            set.add(Arrays.asList(a,b,c,d));
                        }
                    }
                }

            }

        }

        return new ArrayList<>(set);
    }

}
